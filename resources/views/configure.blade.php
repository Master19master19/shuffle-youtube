@extends('layouts.app')

@section('content')
<div class="container">
    <div class="">

        @if ( session( 'status' ) )
            <h2 class="alert alert-danger">{{ session( 'status' ) }}</h2>
        @endif
        @if( $errors -> any() )
            <div class="pb-4">
                @foreach ( $errors -> all() as $error )
                    <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
            </div>
        @endif
        <form class="form" action="{{ route( 'save_config' ) }}" method="POST">
            @csrf
            <div class="form-group">
                <label>Playlist url</label>
                <input autofocus="" value="{{ Auth::user() -> playlist_url ? Auth::user() -> playlist_url : old( 'playlist_url' ) }}" class="form-control @error( 'playlist_url' ) is-invalid @enderror" type="text" required='' minlength="2" name="playlist_url" />
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block">GO</button>
            </div>
        </form>
    </div>
</div>
@endsection
