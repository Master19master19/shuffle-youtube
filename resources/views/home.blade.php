@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <iframe width="100%" style="height:80vh" src="https://www.youtube.com/embed/{{ $playlist }}&autoplay=1&controls=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" autoplay allowfullscreen></iframe>
        </div>
    </div>
</div>
@endsection