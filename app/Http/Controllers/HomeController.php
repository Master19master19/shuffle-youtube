<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConfigureRequest;
use Auth;
use App\User;
use DB;
use App\Playlist;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function test() {
        // https://code.jquery.com/jquery-3.4.1.min.js
//         let arra = [];
// $( '.ytd-playlist-video-list-renderer' ).each( function(){
// let href = $(this).find('a').attr('href');
// if(href) {
// arra.push(href.split('watch?v=')[1].split('&list=')[0])
// }
// })
// console.log(JSON.stringify(arra))
        $contents = file_get_contents( env( 'PARSE_PATH' ) );
        $data = json_decode( $contents );
        foreach ( $data as $key => $video_id ) {
            $dat = [
                'title' => 'parsed',
                'user_id' => 1,
                'thumbnail' => 'https://i.ytimg.com/vi/BhB6Lb7_kN8/hqdefault.jpg',
                'position' => $key,
                'video_id' => $video_id
            ];
            Playlist::updateOrCreate( $dat , [ 
                'video_id' => $dat[ 'video_id' ]
            ]);
        }
        return response( 'ok' );
    }
    public function configure () {
        return view( 'configure' );
    }
    public function save_config ( ConfigureRequest $req ) {
        User::whereId( Auth::user() -> id ) -> update( $req -> except( '_token' ) );
        return redirect() -> route( 'get_videos' );
    }
    private function loopPlaylist ( $apiUrl = null ) {
        $playlistId = Auth::user() -> playlist_url;
        $apiKey = env( 'YOUTUBE_KEY' );
        if ( null == $apiUrl ) {
            $apiUrl = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId={$playlistId}&key={$apiKey}";
        }
        try {
            $playlist = json_decode ( file_get_contents( $apiUrl ) );
        } catch ( \Exception $e ) {
            return redirect() -> route( 'configure' ) -> withStatus( 'Please check if the playlist privacy is public and the id is right' );
        }
        $videos = [];
        foreach ( $playlist -> items as $key => $value ) {
            $info = $value -> snippet;
            $video[ 'title' ] = $info -> title;
            $video[ 'position' ] = $info -> position;
            $video[ 'thumbnail' ] = $info -> thumbnails->high->url;
            $video[ 'video_id' ] = $info -> resourceId -> videoId;
            $video[ 'user_id' ] = Auth::user() -> id;
            Playlist::updateOrCreate( $video , [ 
                'video_id' => $video[ 'video_id' ]
            ]);
            $videos[] = $video;
        }
        if ( isset( $playlist -> nextPageToken ) ) {
            $newApiUrl = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId={$playlistId}&key={$apiKey}&pageToken=" . $playlist -> nextPageToken ;
            $this -> loopPlaylist( $newApiUrl );
        } else {
            return true;
        }
    }
    public function getVideos () {
        $this -> loopPlaylist();
        return redirect() -> route( 'home' );
    }
    public function index () {
        if ( Auth::user() -> playlist_url == null ) {
            return redirect() -> route( 'configure' );
        }
        // $this -> getVideos();
        $videos = Playlist::whereUserId( Auth::user() -> id ) ->orderBy(DB::raw('RAND()')) -> limit(200) -> get() -> toArray();
        if ( ! $videos ) {
            return redirect () -> route( 'get_videos' );
        } else {
            $impl = [];
            foreach ( $videos as $video ) {
                $impl[] = $video[ 'video_id' ];
            }
            array_shift( $impl );
            $playlist = $videos[ 0 ][ 'video_id' ] . "?playlist=" . implode( ',' , $impl );
            return view( 'home' , [ 'videos' => $videos , 'playlist' => $playlist ] );
        }
    }
}




// @foreach( $videos as $video )
//             <div class="col-lg-6 d-none">
//                 <p>{{ $video[ 'title' ] }}</p>
//                 <p><img class="w-100" src="{{ $video[ 'thumbnail' ] }}" /></p>
//                 <iframe width="560" height="315" src="https://www.youtube.com/embed/{{ $video[ 'video_id' ] }}?enablejsapi=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
//             </div>
//         @endforeach